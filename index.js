const express = require('express');
const mongoose = require('mongoose');
const port = 4000;
const app = express();

// Connecting to MongoDB
mongoose.connect("mongodb+srv://orfenoko1:malnour12345@gibz.8x8pf.mongodb.net/session32activity?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "There is an error with the connection"));
db.once("open", () => console.log("Successfully connected to the database"));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Mongoose Schemas
	// schema ===  blueprint of your data

const userSchema = new mongoose.Schema({

	email: String,
	username: String,
	password: String,
	age: Number,
	isAdmin: {
		type: Boolean,
		default: false
	}
});

// Model
const User = mongoose.model("User", userSchema);

// Business Logic

// Creating a new user
app.post("/users/signup", (req, res) => {

	User.findOne({email : req.body.email}, (err, result) => {

		if(result != null && result.email === req.body.email){

			return res.send(`Duplicate user found: ${err}`)

		} else {

			let newUser = new User({
				email : req.body.email,
				username: req.body.username,
				password: req.body.password,
				age: req.body.age
			});

			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr)
				} else {
					return res.status(200).send(`New user registered : ${savedUser}`)
				};
			});
		};
	});
});

// Retrieving all users
app.get("/users", (req, res) => {

	User.find({}, (err, result) => {

		if(err){
			
			return console.log(err);
		
		} else {

			return res.status(200).json({
				users : result
			})
		}
	})
})



// Updating Task Name
app.put("/users/update-user/:userId", (req, res) => {

	let userId = req.params.userId
	let name = req.body.username

	User.findByIdAndUpdate(userId, {username: name}, (err, updatedUsername) => {
		if(err){
			console.log(err)
		} else {
			res.send(`Congratulations, your user name is updated to: ${updatedUsername}`);
		}
	})
})


// Delete user

app.delete("/users/archive-user/:userId", (req,res) => {
	let userId = req.params.userId;
	User.findByIdAndDelete(userId, (err, deletedUser) => {
		if(err){
			console.log(err)
		} else {
			res.send(`User ${deletedUser.username} archived!`)
		}
	})

})

app.listen(port, () => console.log(`Server running at port ${port}`))
